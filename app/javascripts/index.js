function addNewUser() {
    var userTableBody = document.getElementById('user-table-body');
    
    var newLine = document.createElement("tr");
    newLine.innerHTML = '<td><input type="text" name="firstname" value="" /></td><td><input type="text" name="lastname" value="" /></td><td><a href="javascript:void(0)", onclick="removeUser(this)">REMOVE USER</a></td>';
    userTableBody.appendChild(newLine);
}

function removeUser(element) {
    var removableElementsParent = document.getElementById('user-table-body');
    removableElementsParent.removeChild(element.parentElement.parentElement);
}

function submitForm(event) {
    //Prevent submitting the form
    event.preventDefault();
    
    var users = [];
    //Collect invalid data
    var invalidUsers = [];
    
    //It shouldn't be a real life situation, but it could happen when the user is removing an input from the DOM
    if(document.userForm.firstname.length != document.userForm.lastname.length) {
        throw new Error("Error occured during the form processing.")
    }
    
    for (var i = 0 ; i < document.userForm.firstname.length ; i++) {
        var firstname = document.userForm.firstname[i];
        var lastname = document.userForm.lastname[i];
        
        if(!firstname.value || !lastname.value) {
            invalidUsers.push(firstname || lastname);
        } else {
            users.push({
                "firstname": firstname.value,
                "lastname": lastname.value
            });
        }
    }
    
    //Remove lines with invalid data
    invalidUsers.forEach(function(user) {
        removeUser(user);
    });
    
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if(request.readyState == 4) {
            if(request.status == 200) {
                //TODO toast message
                console.log(JSON.parse(request.responseText).result);
            } else if(request.status == 500) {
                //TODO toast message
                console.log(JSON.parse(request.responseText).error);
            }
        }
    }
    request.open('POST', '/users/update');
    request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    request.send(JSON.stringify(users));
}