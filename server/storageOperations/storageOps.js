var path = require('path');
var fs = require('fs');

var defaultPath = path.join(__dirname, '../storage/users.json');

exports.readFile = function(givenPath) {
    return new Promise(function(resolve, reject) {
        fs.readFile(givenPath || defaultPath, 'utf8', function(err, data) {
            if(err) {
                reject("Users load failed with: " + err);
            }
            try {
                resolve(JSON.parse(data));
            } catch (err) {
                reject('JSON parse error: ' + err);
            }
        });
    });
    
}

exports.updateOrCreateFile = function(storableObject, givenPath) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(givenPath || defaultPath, JSON.stringify(storableObject), function(err) {
            reject("Users update failed with: " + err);
        });
        resolve("Users successfully updated.");
    });
}