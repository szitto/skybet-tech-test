describe("storageOps", function() {
    var storageOps = require('../storageOps');
    var path = require('path');
    var fs = require('fs');
    
    var testFilePath = path.join(__dirname, '../../storage/temp.json');
    
    describe("readFile", function() {        
        it("should read and parse the example JSON data from the temp.json", function(done) {
            fs.writeFile(testFilePath, JSON.stringify({"firstname": 'testFirst', lastname: 'testLast'}), function(err) {
                if(err) {
                    throw new Error(err);
                }
            });
            
            storageOps.readFile(testFilePath).then(function(data) {
                expect(data).toEqual({"firstname": 'testFirst', lastname: 'testLast'});
                fs.unlink(testFilePath, done);
            });
        });
        it("should return back with an ENOENT error message when the file is not found", function(done) {
            storageOps.readFile(testFilePath).then(function(data) {}, function(err) {
                expect(err.indexOf("ENOENT") != -1).toBe(true);
                fs.unlink(testFilePath, done);
            });
        });
    });
    
    describe("updateOrCreateFile", function() {
        it("should save the given data correctly", function(done) {
            storageOps.updateOrCreateFile({"testRecord": "testValue"}, testFilePath).then(function(result) {
                storageOps.readFile(testFilePath).then(function(data) {
                    expect(data).toEqual({"testRecord": "testValue"});
                    fs.unlink(testFilePath, done);
                });
            }, function(err) {
                if (err) {
                    throw new Error(err);
                }
            });
        });
    });
});