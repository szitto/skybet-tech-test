var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

//Initialize routes
var index = require(path.join(__dirname, 'routes/index'));
var users = require(path.join(__dirname, 'routes/users'));

var app = express();

//Configure the template engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//Configure app
app.use(bodyParser.json());

//Set static folder
app.use('/static', express.static(path.join('dist')));

//Set routes
app.use('/', index);
app.use('/users', users);

//Start express server
var server = app.listen(3000, function () {
    var port = server.address().port;
    console.info('Server is listening on: ' + port);
});