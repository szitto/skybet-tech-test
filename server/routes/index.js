var express = require('express');
var router = express.Router();
var storageOps = require('../storageOperations/storageOps')

//Set index route
router.get('/', function(req, res) {
    storageOps.readFile().then(function(data) {
        res.render('index', {'users': data.users});
    }, function(err) {
        res.render('index', {'error': err});
    });
});

module.exports = router;