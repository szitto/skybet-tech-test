var express = require('express');
var router = express.Router();
var storageOps = require('../storageOperations/storageOps')

//Set users route
router.post('/update', function(req, res) {
    var storableJson = {"users": req.body};
    
    storageOps.updateOrCreateFile(storableJson).then(function(result) {
        res.status(200).send({'result': result});
    }, function(err) {
        res.status(500).send({'error': err});
    });
});

module.exports = router;