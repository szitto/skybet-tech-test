# Sky Betting & Gaming Technical Test

## Running instructions
Requirements: node v4.0 or higher, because of the ES6

- install dependencies with the 'npm install' command
- use 'npm run start' to start the server in dev mode
- use 'npm run package' to create a build, then run with 'node server/app.js'

- use 'npm run test' to run the unit tests

## Covering note
- I'm using Node.js for the server side which is obvious because I'm also using JavaScript on the client side.
- I'm using Jade for the server side rendering. It's good, because when the user opens the page, then the HTML will be rendered depending on the given data. It's faster and safer than waiting for the server to give back that data to our HTML after it rendered. 
- I'm storing the data in JSON files, which is good because later they could be easily converted to MongoDB documents.
- One of my most important goals are to keep the application as simple as possible, so I didn't use any framework on the client side. Of course, with a framework a lot of things are easier, but at this time it would just make slower the application.
- Currently I've only implemented jasmine tests for the server side file handling methods, but later it could be extended with more unit tests and E2E tests for the client side.
- I tried to create a stucture which could be easily extendable and easy to understand