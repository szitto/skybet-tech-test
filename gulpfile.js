var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var nodemon = require('gulp-nodemon');
var jasmine = require('gulp-jasmine');

gulp.task('sass', function() {
    gulp.src('app/stylesheets/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('dist'));
});

gulp.task('minify', function() {
  gulp.src('app/javascripts/*.js')
      .pipe(minify())
      .pipe(gulp.dest('dist'));
});

gulp.task('unitTest', function() {
    gulp.src('server/**/*.spec.js')
        .pipe(jasmine({
        verbose: true
    }));
});


gulp.task('start', ['sass', 'minify'], function() {
    nodemon({
        script: 'server/app.js',
        ignore: ['gulpfile.js', 'storage/*', 'client/*', 'dist/*']
    });
});

gulp.task('package', ['unitTest', 'sass', 'minify']);

gulp.task('watch', function() {
    gulp.watch('app/stylesheets/*.scss',['sass']);
    gulp.watch('app/javascripts/*.js', ['minify']);
});